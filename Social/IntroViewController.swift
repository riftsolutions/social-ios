//
//  IntroViewController.swift
//  Social
//
//  Created by Ian McDowell on 10/19/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {
    
    var window: UIWindow?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // animated splash view
        let splashView = SplashView(image: UIImage(named: "s_logo")!, backgroundColor: UIColor.white, iconColor: UIColor.black, size: CGSize(width: 250, height: 250))
        self.navigationController?.view.addSubview(splashView)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64((1 * Double(NSEC_PER_SEC)))) / Double(NSEC_PER_SEC)) {
            splashView.animate()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }

    
    @IBAction func next() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroEmailViewController") as! IntroEmailViewController
        nextVC.window = window
        let _ = nextVC.view
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = nextVC
        }, completion: nil)
    }
}


class IntroEmailViewController: UIViewController {
    
    var window: UIWindow?
    
    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var titleLabel: UIView!
    @IBOutlet var continueButton: UIView!
    @IBOutlet var formView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add content types for iOS 10
        if #available(iOS 10.0, *) {
            nameTextField.textContentType = .name
            emailTextField.textContentType = .emailAddress
        }
        titleLabel.alpha = 0
        continueButton.alpha = 0
        formView.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Register for notifications that the keyboard will appear and disappear
        NotificationCenter.default.addObserver(self, selector: #selector(IntroEmailViewController.keyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(IntroEmailViewController.keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        nameTextField.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 1
            self.continueButton.alpha = 1
            self.formView.alpha = 1
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Deregister for keyboard notifications
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func goToEmail() {
        // Make the email text field the first responder
        self.emailTextField.becomeFirstResponder()
    }
    
    
    @IBAction func next() {
        // Validate
        // Dismiss keyboard
        self.nameTextField.resignFirstResponder()
        self.emailTextField.resignFirstResponder()
        
        let name = nameTextField.text ?? ""
        let email = emailTextField.text ?? ""
        
        if name.characters.count == 0 || email.characters.count == 0 || !validateEmail(candidate: email) {
            let alert = UIAlertController(title: "Hey!", message: "Please enter a valid name and email. They are required to use the app.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        // Send info to server
        Fetcher.setUserInfo(email: email, name: name)
        
        // Set onboarded
        Config.hasOnboarded = true
        
        // Exit to main app
        
        let appVC = self.storyboard?.instantiateInitialViewController() as! UINavigationController
        let postsVC = appVC.topViewController as! PostsViewController
        postsVC.hasDisplayedSplashScreen = true
        
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = appVC
        }, completion: nil)
    }
    
    
    func keyboardWillShowNotification(_ notification: Notification) {
        updateBottomLayoutConstraintWithNotification(notification)
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        updateBottomLayoutConstraintWithNotification(notification)
    }
    
    // MARK: - Private
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,9}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    func updateBottomLayoutConstraintWithNotification(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey]! as AnyObject).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        let rawAnimationCurve = ((notification as NSNotification).userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        let animationCurve = UIViewAnimationOptions(rawValue: UInt(rawAnimationCurve))
        
        bottomLayoutConstraint.constant = view.bounds.maxY - convertedKeyboardEndFrame.minY
        
        UIView.animate(withDuration: animationDuration!, delay: 0.0, options: [.beginFromCurrentState, animationCurve], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
