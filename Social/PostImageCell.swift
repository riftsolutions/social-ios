//
//  PostImageCell.swift
//  Social
//
//  Created by Ian McDowell on 7/14/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import Nuke
import FLAnimatedImage
import RealmSwift
import MBCircularProgressBar

class PostImageCell: PostCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var postCollectionView: UICollectionView!
    
    var images = List<ImagePostImage>() {
        didSet {
            if images.count == 0 {
                self.aspectConstraint = nil
            } else {
                let averageAspect = PostImageCell.collectionViewRatioForImages(images)
                
                self.aspectConstraint = NSLayoutConstraint(item: postCollectionView, attribute: .width, relatedBy: .equal, toItem: postCollectionView, attribute: .height, multiplier: averageAspect, constant: 0)
            }
            
            self.postCollectionView.reloadData()
        }
    }
    
    fileprivate var aspectConstraint: NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                postCollectionView.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                postCollectionView.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func setup() {
        super.setup()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets.zero
        
        self.postCollectionView = {
            let c = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
            c.translatesAutoresizingMaskIntoConstraints = false
            
            c.backgroundColor = UIColor.clear
            
            c.isPagingEnabled = true
            c.dataSource = self
            c.delegate = self
            
            self.postContentView.addSubview(c)
            
            c.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor).isActive = true
            c.trailingAnchor.constraint(equalTo: self.postContentView.trailingAnchor).isActive = true
            c.topAnchor.constraint(equalTo: self.postContentView.topAnchor).isActive = true
            c.bottomAnchor.constraint(equalTo: self.postContentView.bottomAnchor).isActive = true
            
            c.register(PostImageCollectionCell.self, forCellWithReuseIdentifier: NSStringFromClass(PostImageCollectionCell.self))
            
            return c
        }()
    }
    
    override func setPost(_ post: Post) {
        super.setPost(post)
        
        self.images = post.imageData!.images
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.images = List<ImagePostImage>()
        aspectConstraint = nil
    }
    
    // Update this when adjusting layout
    override class func estimatedHeightForPost(_ post: Post, tableView: UITableView) -> CGFloat {
        
        let ratio = collectionViewRatioForImages(post.imageData!.images)
        let width = tableView.frame.size.width - Config.postSpacing * 2 - Config.postPadding * 2
        let height = ratio == 0 ? 0 : width / ratio
        
        return super.estimatedHeightForPost(post, tableView: tableView) + height
    }
    
    // mark - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(PostImageCollectionCell.self), for: indexPath) as! PostImageCollectionCell
        
        cell.setPostImage(images[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.currentDotsView?.currentPage = (indexPath as NSIndexPath).row
    }
    
    // mark - UICollectionViewDelegateFlowlayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    // Averages the aspects of the images
    fileprivate class func collectionViewRatioForImages(_ images: List<ImagePostImage>!) -> CGFloat {
        if images.count == 0 {
            return 0
        }
        
        var aspects = [CGFloat]()
        for image in images {
            aspects.append(CGFloat(image.width / image.height))
        }
        return aspects.reduce(0, +) / CGFloat(aspects.count)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateDots()
    }
    
    func updateDots() {
        if let indexPath = self.postCollectionView.indexPathForItem(at: self.postCollectionView.convert(self.postCollectionView.center, from: self.delegate?.getView())) {
            if self.currentDotsView?.currentPage != (indexPath as NSIndexPath).row {
                self.currentDotsView?.currentPage = (indexPath as NSIndexPath).row
            }
        }
    }
    
}


class PostImageCollectionCell: UICollectionViewCell {
    
    var postImageView: FLAnimatedImageView!
    var loadingView: MBCircularProgressBarView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        
        self.postImageView = {
            let i = FLAnimatedImageView()
            i.translatesAutoresizingMaskIntoConstraints = false
            i.contentMode = .scaleAspectFit
            
            self.contentView.addSubview(i)
            
            i.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
            i.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
            i.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
            i.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
            
            return i
        }()
        
        self.loadingView = {
            let p = MBCircularProgressBarView()
            p.translatesAutoresizingMaskIntoConstraints = false
            
            // TODO: Make indeterminate
            p.value = 0
            p.maxValue = 1
            p.showUnitString = false
            p.showValueString = false
            p.emptyLineColor = UIColor(white: 0.8, alpha: 1)
            p.emptyLineWidth = 5
            p.progressCapType = 2
            p.progressLineWidth = 5
            p.progressStrokeColor = UIColor.clear
            p.progressColor = UIColor(white: 0.6, alpha: 1)
            p.progressAngle = 100
            
            p.backgroundColor = UIColor.clear
            
            self.contentView.addSubview(p)
            p.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            p.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
            p.widthAnchor.constraint(equalToConstant: 50).isActive = true
            p.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            return p
        }()
    }
    
    func setPostImage(_ image: ImagePostImage) {
        
        let imageURL = URL(string: image.url, relativeTo: URL(string: Config.baseURL))!
        
        self.loadingView.isHidden = false
        Nuke.loadImage(with: imageURL, into: self.postImageView) { [weak postImageView] in
            postImageView?.handle(response: $0, isFromMemoryCache: $1)
            self.loadingView.isHidden = true
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        postImageView.image = nil
        
        Nuke.cancelRequest(for: self.postImageView)
    }
}
