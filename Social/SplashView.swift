//
//  SplashView.swift
//  Social
//
//  Created by Ian McDowell on 7/24/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit

class SplashView: UIView {

    fileprivate var iconImageView: UIImageView!
    
    init(image: UIImage, backgroundColor: UIColor, iconColor: UIColor, size: CGSize) {
        super.init(frame: UIScreen.main.bounds)
        
        self.backgroundColor = backgroundColor
        
        self.iconImageView = {
            let iv = UIImageView()
            
            iv.image = image
            iv.tintColor = iconColor
            iv.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            iv.contentMode = .scaleAspectFit
            iv.center = self.center
            
            return iv
        }()
        
        self.addSubview(iconImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func animate() {
        
        let shrinkDuration = 0.3
        let growDuration = 0.7
        
        UIView.animate(withDuration: shrinkDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: UIViewAnimationOptions(), animations: { 
            
            let scaleTransform = CGAffineTransform(scaleX: 0.75, y: 0.75)
            self.iconImageView.transform = scaleTransform
            
        }) { (finished) in
            UIView.animate(withDuration: growDuration, animations: {
                
                let scaleTransform = CGAffineTransform(scaleX: 20, y: 20)
                self.iconImageView.transform = scaleTransform
                self.alpha = 0
                
            }, completion: { (finish) in
                self.removeFromSuperview()
            })
        }
    }

}
