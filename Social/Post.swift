//
//  Post.swift
//  Social
//
//  Created by Ian McDowell on 7/14/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import ObjectMapper
import RealmSwift

let PostDataTypeImage = 1
let PostDataTypeVideo = 2
let PostDataTypeText = 3
let PostDataTypeQA = 4

class Post: Object, Mappable {
    
    dynamic var id: String!
    dynamic var title: String = ""
    dynamic var caption: String = ""
    dynamic var date: Date!
    dynamic var likes: Int = 0
    dynamic var views: Int = 0
    
    dynamic var dataType: Int = -1
    
    dynamic var cta: CTAData? // call to action
    
    dynamic var imageData: ImagePostData?
    dynamic var videoData: VideoPostData?
    dynamic var textData: TextPostData?
    dynamic var qaData: QAPostData?
    
    dynamic fileprivate var hasViewed = false
    dynamic var hasLiked = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["_id"]
        title <- map["title"]
        caption <- map["caption"]
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        date <- (map["date"], DateFormatterTransform(dateFormatter: formatter))
        likes <- map["likes"]
        views <- map["views"]
        
        dataType <- map["dataType"]
        
        cta <- map["cta"]
        
        switch dataType {
        case PostDataTypeImage:
            self.imageData <- map["data"]
            break
        case PostDataTypeVideo:
            self.videoData <- map["data"]
            break
        case PostDataTypeText:
            self.textData <- map["data"]
            break
        case PostDataTypeQA:
            self.qaData <- map["data"]
            break
        default:
            break
        }

        let predicate = NSPredicate(format: "id = %@", self.id)
        if let post = try! Realm().objects(Post.self).filter(predicate).first {
            self.hasViewed = post.hasViewed
            self.hasLiked = post.hasLiked
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func view() {
        try! Realm().write {
            if !hasViewed {
                views = views + 1
                hasViewed = true
            
                Fetcher.viewPost(self)
            }
        }
    }
    
    func like() {
        try! Realm().write {
            if !hasLiked {
                likes = likes + 1
                hasLiked = true
            
                Fetcher.likePost(self)
            }
        }
    }
    
    func unlike() {
        try! Realm().write {
            if hasLiked {
                likes = likes - 1
                hasLiked = false
            
                Fetcher.unlikePost(self)
            }
        }
    }
}

class ImagePostData: Object, Mappable {
    
    let images = List<ImagePostImage>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        // Map all images into the images array
        if let imgs = map["images"].currentValue as? [AnyObject] {
            for img in imgs {
                if let i = Mapper<ImagePostImage>().map(JSONObject: img) {
                    images.append(i)
                }
            }
        }
    }
}
class ImagePostImage: Object, Mappable {
    dynamic var url: String = ""
    dynamic var width: Float = 0
    dynamic var height: Float = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        width <- map["width"]
        height <- map["height"]
    }
}
class VideoPostData: Object, Mappable {
    dynamic var url: String = ""
    dynamic var width: Float = 0
    dynamic var height: Float = 0
    dynamic var loops: Bool = false
    dynamic var controls: Bool = true
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        width <- map["width"]
        height <- map["height"]
        loops <- map["loops"]
        controls <- map["controls"]
    }
}
class TextPostData: Object, Mappable {
    dynamic var text: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        text <- map["text"]
    }
}
class QAPostData: Object, Mappable {
    dynamic var question: String = ""
    dynamic var answer: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        question <- map["question"]
        answer <- map["answer"]
    }
}

class CTAData: Object, Mappable {
    dynamic var title: String = ""
    dynamic var url: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        url <- map["url"]
    }
}
