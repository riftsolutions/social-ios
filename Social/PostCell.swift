//
//  PostCell.swift
//  Social
//
//  Created by Ian McDowell on 7/14/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit

protocol PostCellDelegate {
    func sharePost(_ post: Post)
    func openCTAForPost(_ post: Post)
    func getView() -> UIView
}

class PostCell: UITableViewCell {
    
    class func registerCellsWithTableView(_ tableView: UITableView) {
        let cellClasses = [PostCell.self, PostImageCell.self, PostVideoCell.self, PostTextCell.self, PostQACell.self]
        
        for cellClass in cellClasses {
            tableView.register(cellClass, forCellReuseIdentifier: NSStringFromClass(cellClass))
        }
    }
    
    class func cellClassForPost(_ post: Post) -> PostCell.Type {
        switch post.dataType {
        case PostDataTypeImage:
            return PostImageCell.self
        case PostDataTypeVideo:
            return PostVideoCell.self
        case PostDataTypeText:
            return PostTextCell.self
        case PostDataTypeQA:
            return PostQACell.self
        default:
            return PostCell.self
        }
    }
    class func cellIdentifierForPost(_ post: Post) -> String {
        return NSStringFromClass(cellClassForPost(post))
    }
    
    // Update this when adjusting layout
    class func estimatedHeightForPost(_ post: Post, tableView: UITableView) -> CGFloat {
        var height: CGFloat = 92
        
        if post.cta != nil {
            height += PostCTAView.requestedHeight
        } else if post.imageData != nil && post.imageData!.images.count > 1 {
            height += PostDotsView.requestedHeight
        }
        
        return height
    }
    
    var delegate: PostCellDelegate?
    
    var currentPost: Post?
    var active = false
    
    fileprivate var postContainer: UIView!

    var postContentView: UIView!
    fileprivate var postPeripheralsBar: UIView!
    fileprivate var postCTAView: PostCTAView!
    fileprivate var postDotsView: PostDotsView!
    var postLikeButton: LikeButton!
    var postShareButton: UIButton!
    var postDateLabel: UILabel!
    var postTitleLabel: UILabel!
    var postCaptionLabel: UILabel!
    
    var currentDotsView: DotsViewProtocol?
    
    fileprivate var postDateFormatter: DateFormatter!
    
    fileprivate var postTitleLabelTop: NSLayoutConstraint?
    fileprivate var postCaptionLabelTop: NSLayoutConstraint?
    fileprivate var postCaptionLabelBottom: NSLayoutConstraint?
    fileprivate var postPeripheralsBarHeight: NSLayoutConstraint?
    
    let buttonColor = UIColor(red: 136/255, green: 153/255, blue: 166/255, alpha: 1.0)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        
        postDateFormatter = DateFormatter()
        postDateFormatter.dateStyle = .short
        postDateFormatter.timeStyle = .short
        postDateFormatter.doesRelativeDateFormatting = true
        
        self.postContainer = {
            let v = UIView()
            v.translatesAutoresizingMaskIntoConstraints = false
            
            v.backgroundColor = UIColor.white
            
            v.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
            v.layer.shadowOffset = CGSize(width: 0, height: 0)
            v.layer.shadowOpacity = 1
            v.layer.shadowRadius = 2
            v.layer.masksToBounds = false
            v.layer.borderColor = UIColor(white: 0.95, alpha: 1).cgColor
            v.layer.borderWidth = 0.5
            
            self.contentView.addSubview(v)
            
            v.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0).isActive = true
            v.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0).isActive = true
            v.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
            v.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -Config.postMargin).isActive = true
            
            return v
        }()
        
        self.postContentView = {
            let v = UIView()
            v.translatesAutoresizingMaskIntoConstraints = false
            
            v.isUserInteractionEnabled = true
            v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PostCell.contentViewTapped)))
            
            self.postContainer.addSubview(v)
            
            v.leadingAnchor.constraint(equalTo: self.postContainer.leadingAnchor).isActive = true
            v.trailingAnchor.constraint(equalTo: self.postContainer.trailingAnchor).isActive = true
            v.topAnchor.constraint(equalTo: self.postContainer.topAnchor).isActive = true
            
            return v
        }()
        
        self.postPeripheralsBar = {
            let v = UIView()
            v.translatesAutoresizingMaskIntoConstraints = false
            
            v.isHidden = true
            v.addBorders(top: true, bottom: true, color: UIColor(white: 235/255, alpha: 1), inset: Config.postPadding)
            
            self.postContainer.addSubview(v)
            
            v.leadingAnchor.constraint(equalTo: self.postContainer.leadingAnchor).isActive = true
            v.trailingAnchor.constraint(equalTo: self.postContainer.trailingAnchor).isActive = true
            v.topAnchor.constraint(equalTo: self.postContentView.bottomAnchor).isActive = true
            self.postPeripheralsBarHeight = v.heightAnchor.constraint(equalToConstant: 0)
            self.postPeripheralsBarHeight?.isActive = true
            
            return v
        }()

        self.postCTAView = PostCTAView()
        self.postCTAView.translatesAutoresizingMaskIntoConstraints = false
        self.postCTAView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PostCell.tappedCTAButton)))
        
        self.postDotsView = PostDotsView()
        self.postDotsView.translatesAutoresizingMaskIntoConstraints = false
        
        let postActionsBar: UIView = {
            let v = UIView()
            v.translatesAutoresizingMaskIntoConstraints = false
            
            self.postContainer.addSubview(v)
            
            v.leadingAnchor.constraint(equalTo: self.postContainer.leadingAnchor).isActive = true
            v.trailingAnchor.constraint(equalTo: self.postContainer.trailingAnchor).isActive = true
            v.topAnchor.constraint(equalTo: postPeripheralsBar.bottomAnchor).isActive = true
            v.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            return v
        }()
        
        self.postLikeButton = {
            let b = LikeButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44), image: UIImage(named: "heart")!)
            b.translatesAutoresizingMaskIntoConstraints = false
            
            b.imageColorOff = buttonColor
            b.imageColorOn = UIColor(red: 254/255, green: 110/255, blue: 111/255, alpha: 1.0)
            b.circleColor = UIColor(red: 254/255, green: 110/255, blue: 111/255, alpha: 1.0)
            b.lineColor = UIColor(red: 226/255, green: 96/255, blue: 96/255, alpha: 1.0)
            b.addTarget(self, action: #selector(PostCell.tappedLikeButton), for: .touchUpInside)
            
            postActionsBar.addSubview(b)
            
            b.widthAnchor.constraint(equalToConstant: 44).isActive = true
            b.leadingAnchor.constraint(equalTo: postActionsBar.leadingAnchor).isActive = true
            b.topAnchor.constraint(equalTo: postActionsBar.topAnchor).isActive = true
            b.bottomAnchor.constraint(equalTo: postActionsBar.bottomAnchor).isActive = true
            
            return b
        }()
        
        self.postShareButton = {
            let b = UIButton.init(type: .system)
            b.translatesAutoresizingMaskIntoConstraints = false
            
            b.setImage(UIImage(named: "share")!, for: UIControlState())
            b.setTitle("Share", for: UIControlState())
            b.tintColor = buttonColor
            
            // move image up a bit
            b.imageEdgeInsets = UIEdgeInsetsMake(-3, 0, 0, 0)
            // move text to the right
            b.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, -7)
            
            b.addTarget(self, action: #selector(PostCell.tappedShareButton), for: .touchUpInside)
            
            postActionsBar.addSubview(b)
            
            b.leadingAnchor.constraint(equalTo: postLikeButton.trailingAnchor).isActive = true
            b.topAnchor.constraint(equalTo: postActionsBar.topAnchor).isActive = true
            b.bottomAnchor.constraint(equalTo: postActionsBar.bottomAnchor).isActive = true
            
            return b
        }()
        
        self.postDateLabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular)
            l.textAlignment = .right
            
            l.textColor = UIColor(red: 197/255, green: 197/255, blue: 197/255, alpha: 1)
            
            postActionsBar.addSubview(l)
            
            l.trailingAnchor.constraint(equalTo: postActionsBar.trailingAnchor, constant: -Config.postPadding).isActive = true
            l.topAnchor.constraint(equalTo: postActionsBar.topAnchor).isActive = true
            l.bottomAnchor.constraint(equalTo: postActionsBar.bottomAnchor).isActive = true
            
            return l
        }()
        
        self.postTitleLabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightBold)
            
            l.textColor = Config.textColor
            
            self.postContainer.addSubview(l)
            
            l.leadingAnchor.constraint(equalTo: self.postContainer.leadingAnchor, constant: Config.postPadding).isActive = true
            l.trailingAnchor.constraint(equalTo: self.postContainer.trailingAnchor, constant: -Config.postPadding).isActive = true
            self.postTitleLabelTop = l.topAnchor.constraint(equalTo: postActionsBar.bottomAnchor, constant: Config.postSpacing)
            self.postTitleLabelTop?.isActive = true
            
            return l
        }()
        
        self.postCaptionLabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)
            
            l.textColor = Config.textColor
            
            self.postContainer.addSubview(l)
            
            l.leadingAnchor.constraint(equalTo: self.postContainer.leadingAnchor, constant: Config.postPadding).isActive = true
            l.trailingAnchor.constraint(equalTo: self.postContainer.trailingAnchor, constant: -Config.postPadding).isActive = true
            self.postCaptionLabelTop = l.topAnchor.constraint(equalTo: self.postTitleLabel.bottomAnchor, constant: Config.postSpacing)
            self.postCaptionLabelTop?.isActive = true
            self.postCaptionLabelBottom = l.bottomAnchor.constraint(equalTo: self.postContainer.bottomAnchor, constant: -Config.postPadding)
            self.postCaptionLabelBottom?.isActive = true
            
            return l
        }()
    }
    
    func setPost(_ post: Post) {
        self.currentPost = post
        
        self.postDateLabel.text = postDateFormatter.string(from: post.date as Date)
        
        self.postTitleLabel.text = post.title
        
        let title = post.title.characters.count != 0
        if title {
            self.postTitleLabelTop?.constant = Config.postSpacing
        } else {
            self.postTitleLabelTop?.constant = 0
        }
        
        self.postCaptionLabel.text = post.caption
        
        let caption = post.caption.characters.count != 0
        if caption {
            self.postCaptionLabelTop?.constant = Config.postSpacing
        } else {
            self.postCaptionLabelTop?.constant = 0
        }
        
        if !title && !caption {
            self.postCaptionLabelBottom?.constant = 0
        } else {
            self.postCaptionLabelBottom?.constant = -Config.postPadding
        }
        
        if post.hasLiked {
            postLikeButton.select()
        } else {
            postLikeButton.deselect()
        }
        
        if post.cta != nil {
            showCTAView()
            self.postCTAView.setPost(post)
            currentDotsView = self.postCTAView
        } else if post.imageData != nil && post.imageData!.images.count > 1 {
            showDotsView()
            self.postDotsView.setPost(post)
            currentDotsView = self.postDotsView
        } else {
            self.postPeripheralsBar.isHidden = true
        }
    }
    
    func activate() {
        if (!active) {
            NSLog("Cell activating: %@", self.currentPost!.id)
            
            active = true
        }
    }
    
    func deactivate() {
        if (active) {
            NSLog("Cell deactivating: %@", self.currentPost!.id)
            
            active = false
        }
    }
    
    // When content view is tapped.
    func action() {
        
    }
    
    @objc fileprivate func contentViewTapped() {
        if self.currentPost == nil {
            return
        }
        
        NSLog("Content view tapped: %@", self.currentPost!.id)
        action();
    }
    
    @objc fileprivate func tappedLikeButton() {
        if self.currentPost == nil {
            return
        }
        
        if self.currentPost!.hasLiked {
            self.currentPost!.unlike()
            postLikeButton.deselect()
        } else {
            self.currentPost!.like()
            postLikeButton.select()
        }
    }
    
    @objc fileprivate func tappedShareButton() {
        if self.currentPost == nil {
            return
        }
        self.delegate?.sharePost(self.currentPost!)
    }
    
    @objc fileprivate func tappedCTAButton() {
        if self.currentPost == nil {
            return
        }
        self.delegate?.openCTAForPost(self.currentPost!)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.currentPost = nil
        
        self.postTitleLabel.text = nil
        self.postDateLabel.text = nil
        self.postCaptionLabel.text = nil
        
        clearPeripheralsBar()
        self.currentDotsView = nil
        self.postCTAView.prepareForReuse()
    }
    
    fileprivate func showCTAView() {
        self.postPeripheralsBar.isHidden = false
        clearPeripheralsBar()
        
        postPeripheralsBarHeight?.constant = PostCTAView.requestedHeight
        self.postPeripheralsBar.addSubview(postCTAView)
        postCTAView.constrainToEdgesOfSuperview()
    }
    fileprivate func showDotsView() {
        self.postPeripheralsBar.isHidden = false
        clearPeripheralsBar()
        
        postPeripheralsBarHeight?.constant = PostDotsView.requestedHeight
        self.postPeripheralsBar.addSubview(postDotsView)
        postDotsView.constrainToEdgesOfSuperview()
    }
    fileprivate func clearPeripheralsBar() {
        postPeripheralsBarHeight?.constant = 0
        for view in self.postPeripheralsBar.subviews {
            if view.tag != 1 { // don't remove separator
                view.removeFromSuperview()
            }
        }
    }
}

protocol DotsViewProtocol {
    var currentPage: Int {get set}
}

private class PostCTAView: UIView, DotsViewProtocol {
    
    static let requestedHeight: CGFloat = 33
    
    fileprivate var titleLabel: UILabel
    fileprivate var dotsView: PostDotsView
    
    var currentPage: Int {
        set {
            dotsView.currentPage = newValue
        }
        get {
            return dotsView.currentPage
        }
    }
    
    init() {
        titleLabel = UILabel()
        dotsView = PostDotsView()
        
        super.init(frame: CGRect.zero)
        
        titleLabel.textColor = UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1) // blue link color
        
        self.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Config.postPadding).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(dotsView)
        dotsView.translatesAutoresizingMaskIntoConstraints = false
        dotsView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Config.postPadding).isActive = true
        dotsView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        titleLabel.trailingAnchor.constraint(equalTo: dotsView.leadingAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPost(_ post: Post) {
        self.titleLabel.text = post.cta?.title
        self.dotsView.numberOfPages = post.imageData?.images.count ?? 0
    }
    
    func prepareForReuse() {
        self.titleLabel.text = nil
    }
}

private class PostDotsView: UIPageControl, DotsViewProtocol {
    static let requestedHeight: CGFloat = 22
    
    init() {
        super.init(frame: CGRect.zero)
        
        self.pageIndicatorTintColor = UIColor.lightGray
        self.currentPageIndicatorTintColor = UIColor.darkGray
        
        self.isUserInteractionEnabled = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPost(_ post: Post) {
        self.numberOfPages = post.imageData?.images.count ?? 0
    }
}

