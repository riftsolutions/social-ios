//
//  AppDelegate.swift
//  Social
//
//  Created by Ian McDowell on 7/9/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import AVFoundation

import Alamofire
import Nuke
import NukeAlamofirePlugin
import NukeFLAnimatedImagePlugin
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // dont stop music playback when gifs play
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        
        // Nuke setup
        let decoder = Nuke.DataDecoderComposition(decoders: [AnimatedImageDecoder(), Nuke.DataDecoder()])
        let cache = Nuke.Cache().preparedForAnimatedImages()
        let loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader(), decoder: decoder, cache: cache)
        Nuke.Manager.shared = Nuke.Manager(loader: loader, cache: cache)
        
        // Realm setup
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    migration.deleteData(forType: "Post")
                }
            }
        )
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config

        
        // Register launch
        Fetcher.registerLaunch()
        
        NSLog("Social app launched.");
        #if DEBUG
            NSLog("Running in DEBUG mode.");
        #else
            NSLog("Running in RELEASE mode.");
        #endif

        UINavigationBar.appearance().tintColor = UIColor.black
        
        
        if !Config.hasOnboarded {
            NSLog("Onboarding user.")
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
            
            vc.window = self.window
            window?.rootViewController = vc
        }
        
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let pathComponents = userActivity.webpageURL?.pathComponents {
                if pathComponents.count > 2 && pathComponents[pathComponents.count - 2] == "share" {
                    if let postID = pathComponents.last {
                        
                        ((window?.rootViewController as? UINavigationController)?.viewControllers.first as? PostsViewController)?.showPost(postID)
                        
                        return true
                    }
                }
            }
        }
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

