//
//  ViewController.swift
//  Social
//
//  Created by Ian McDowell on 7/9/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import SafariServices

import HidingNavigationBar

import RealmSwift

class PostsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, PostCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var hidingNavBarManager: HidingNavigationBarManager?
    
    var postIDToScrollTo: String? = nil
    
    var hasDisplayedSplashScreen: Bool = false
    
    fileprivate var posts: Results<Post>? = nil {
        didSet {
            self.tableView.reloadData()
            self.activateCells()
            
            self.scrollToPostIfNeeded()
        }
    }
    
    var notificationToken: NotificationToken? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "s_logo_title"))
        self.navigationItem.title = " "
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(PostsViewController.messageButtonTapped))
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // hiding navigation bar on scroll
        hidingNavBarManager = HidingNavigationBarManager(viewController: self, scrollView: tableView)
        
        // Register cells for reuse
        PostCell.registerCellsWithTableView(self.tableView)
        
        // Refresh control
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(PostsViewController.reload), for: .valueChanged)
        self.tableView.addSubview(self.refreshControl)
        hidingNavBarManager?.refreshControl = refreshControl
        
        
        // reload when app is relaunched from memory
        NotificationCenter.default.addObserver(self, selector: #selector(PostsViewController.reload), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        // Get posts
        let realm = try! Realm()
        posts = realm.objects(Post.self).sorted(byProperty: "date", ascending: false)
        
        // Observe Results Notifications
        notificationToken = posts!.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, _):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map { IndexPath(row: $0, section: 0) },
                    with: .automatic)
                tableView.deleteRows(at: deletions.map { IndexPath(row: $0, section: 0) },
                    with: .automatic)
//                tableView.reloadRowsAtIndexPaths(modifications.map { NSIndexPath(forRow: $0, inSection: 0) },
//                    withRowAnimation: .Automatic)
                tableView.endUpdates()
                
                self?.scrollToPostIfNeeded()
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                break
            }
        }
    }
    
    deinit {
        notificationToken?.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Load posts
        reload()
        
        hidingNavBarManager?.viewWillAppear(animated)
        
        if !hasDisplayedSplashScreen {
            // animated splash view
            let splashView = SplashView(image: UIImage(named: "s_logo")!, backgroundColor: UIColor.white, iconColor: UIColor.black, size: CGSize(width: 250, height: 250))
            self.navigationController?.view.addSubview(splashView)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64((1 * Double(NSEC_PER_SEC)))) / Double(NSEC_PER_SEC)) {
                splashView.animate()
                
                self.hasDisplayedSplashScreen = true
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        hidingNavBarManager?.viewDidLayoutSubviews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hidingNavBarManager?.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func messageButtonTapped() {
        let messageVC = MessageViewController()
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    
    func showPost(_ postID: String) {
        self.postIDToScrollTo = postID
        self.scrollToPostIfNeeded()
    }
    
    func scrollToPostIfNeeded() {
        if posts == nil {
            return
        }
        if let postID = self.postIDToScrollTo {
            for post in posts! {
                if post.id == postID {
                    if let index = posts?.index(of: post) {
                        self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
                    }
                    self.postIDToScrollTo = nil
                    
                    return
                }
            }
            
        }
    }
    
    @objc func reload() {
        var finished = false
        
        // Wait a bit, as to not fuck up the shy nav bar -_-
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5) * Int64(NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            if !finished {
                self.refreshControl.beginRefreshing()
            }
        }
        
        Fetcher.getPosts { (error) in
            finished = true
            
            if error != nil || self.posts?.count ?? 0 == 0 {
                let alert = UIAlertController(title: "Error loading posts", message: "Unable to load posts. Try again later.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: PostCellDelegate
    func sharePost(_ post: Post) {
        
        let url = URL(string: "\(Config.baseShareURL)\(post.id)")
        
        NSLog("Share url: \(url)")
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        
        self.present(activityViewController, animated: true, completion: nil)

    }
    
    func openCTAForPost(_ post: Post) {
        if let urlString = post.cta?.url, let url = URL(string: urlString) {
            let safariVC = SFSafariViewController(url: url)
            self.present(safariVC, animated: true, completion: nil)
        }
        
    }
    
    func getView() -> UIView {
        return view
    }
    
    // MARK: UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = posts![(indexPath as NSIndexPath).row]
        let identifier = PostCell.cellIdentifierForPost(post)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PostCell
        
        cell.setPost(post)
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return PostCell.cellClassForPost(posts![(indexPath as NSIndexPath).row]).estimatedHeightForPost(posts![(indexPath as NSIndexPath).row], tableView: tableView)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        posts![(indexPath as NSIndexPath).row].view()
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if (!scrollView.decelerating && !scrollView.dragging) {
            self.activateCells()
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (!decelerate) {
            self.activateCells()
        }
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        hidingNavBarManager?.shouldScrollToTop()
        
        return true
    }

    // MARK: Cell activation
    
    func activateCells() {
        if (posts == nil || posts!.count == 0) {
            return
        }
        
        if let centerIndex = self.tableView.indexPathForRow(at: self.tableView.convert(self.tableView.center, from: self.view)) {
            let centerCell = self.tableView.cellForRow(at: centerIndex)
            for cell in self.tableView.visibleCells {
                
                if cell == centerCell {
                    (cell as? PostCell)?.activate()
                } else {
                    (cell as? PostCell)?.deactivate()
                }
            }
        }

    }
}

