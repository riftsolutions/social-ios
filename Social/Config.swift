//
//  Config.swift
//  Social
//
//  Created by Ian McDowell on 7/14/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit

struct Config {

    static let baseURL = "https://seandshoots.rift.solutions"
    
    static let baseAPIURL = "\(baseURL)/api/1/"
    static let baseShareURL = "\(baseURL)/share/" // requires ID after
    
    struct Endpoints {
        static let posts = "posts"
        static let views = "views"
        static let like = "like"
        static let unlike = "unlike"
        static let registerLaunch = "registerLaunch"
        static let message = "message"
        static let messages = "messages"
        static let userInfo = "userInfo"
    }
    
    
    static let postMargin: CGFloat = 7 // margin around posts
    static let postPadding: CGFloat = 12 // padding around post content
    static let postSpacing: CGFloat = 5 // spacing between contents of post
    
    static let textColor = UIColor(white: 89/255, alpha: 1)
    
    
    static var hasOnboarded: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "onboarded")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "onboarded")
        }
    }
}



extension UIView {
    
    func constrainToEdgesOfSuperview() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.leadingAnchor.constraint(equalTo: (self.superview?.leadingAnchor)!).isActive = true
        self.trailingAnchor.constraint(equalTo: (self.superview?.trailingAnchor)!).isActive = true
        self.topAnchor.constraint(equalTo: (self.superview?.topAnchor)!).isActive = true
        self.bottomAnchor.constraint(equalTo: (self.superview?.bottomAnchor)!).isActive = true
    }
    
    func addBorders(top: Bool = false, left: Bool = false, bottom: Bool = false, right: Bool = false, color: UIColor = UIColor.lightGray, inset: CGFloat = 0) {
        
        if top {
            var topBorder = generateBorder(color)
            self.addSubview(topBorder)
            setupTopOrBottomBorder(&topBorder, inset)
            topBorder.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        }
        if bottom {
            var bottomBorder = generateBorder(color)
            self.addSubview(bottomBorder)
            setupTopOrBottomBorder(&bottomBorder, inset)
            bottomBorder.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
        if left {
            var leftBorder = generateBorder(color)
            self.addSubview(leftBorder)
            setupLeftOrRightBorder(&leftBorder, inset)
            leftBorder.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        }
        if right {
            var rightBorder = generateBorder(color)
            self.addSubview(rightBorder)
            setupLeftOrRightBorder(&rightBorder, inset)
            rightBorder.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        }
    }
    
    fileprivate func generateBorder(_ color: UIColor) -> UIView {
        let b = UIView()
        b.backgroundColor = color
        b.tag = 1
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }
    
    fileprivate func setupTopOrBottomBorder(_ border: inout UIView, _ inset: CGFloat) {
        border.heightAnchor.constraint(equalToConstant: 1).isActive = true
        border.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: inset).isActive = true
        border.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -inset).isActive = true
    }
    
    fileprivate func setupLeftOrRightBorder(_ border: inout UIView, _ inset: CGFloat) {
        border.widthAnchor.constraint(equalToConstant: 1).isActive = true
        border.topAnchor.constraint(equalTo: self.topAnchor, constant: inset).isActive = true
        border.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -inset).isActive = true
    }
    
}
