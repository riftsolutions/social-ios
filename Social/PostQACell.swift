//
//  PostQACell.swift
//  Social
//
//  Created by Ian McDowell on 8/19/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit

class PostQACell: PostCell {
    
    var postQuestionLabel: UILabel!
    var postAnswerLabel: UILabel!
    
    override func setup() {
        super.setup()
        
        let qaLabel: UILabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightLight)
            l.textColor = buttonColor
            
            l.text = "Q & A"
            
            self.postContentView.addSubview(l)
            
            l.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor, constant: Config.postPadding).isActive = true
            l.trailingAnchor.constraint(equalTo: self.postContentView.trailingAnchor, constant: -Config.postPadding).isActive = true
            l.topAnchor.constraint(equalTo: self.postContentView.topAnchor, constant: Config.postPadding).isActive = true
            
            return l
        }()
        
        self.postQuestionLabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.numberOfLines = 0
            l.lineBreakMode = .byWordWrapping
            
            l.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
            l.textColor = Config.textColor
            
            self.postContentView.addSubview(l)
            
            l.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor, constant: Config.postPadding).isActive = true
            l.trailingAnchor.constraint(equalTo: self.postContentView.trailingAnchor, constant: -Config.postPadding).isActive = true
            l.topAnchor.constraint(equalTo: qaLabel.bottomAnchor, constant: Config.postPadding).isActive = true
            
            return l
        }()
        
        let divider: UIView = {
            let d = UIView()
            d.translatesAutoresizingMaskIntoConstraints = false
            
            d.backgroundColor = UIColor(white: 0.9, alpha: 1)
            
            self.postContentView.addSubview(d)
            
            d.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor, constant: Config.postPadding).isActive = true
            d.topAnchor.constraint(equalTo: self.postQuestionLabel.bottomAnchor, constant: Config.postPadding).isActive = true
            d.heightAnchor.constraint(equalToConstant: 1).isActive = true
            d.widthAnchor.constraint(equalToConstant: 60).isActive = true
            
            return d
        }()
        
        self.postAnswerLabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.numberOfLines = 0
            l.lineBreakMode = .byWordWrapping
            
            l.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
            l.textColor = Config.textColor
            
            self.postContentView.addSubview(l)
            
            l.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor, constant: Config.postPadding).isActive = true
            l.trailingAnchor.constraint(equalTo: self.postContentView.trailingAnchor, constant: -Config.postPadding).isActive = true
            l.topAnchor.constraint(equalTo: divider.bottomAnchor, constant: Config.postPadding).isActive = true
            l.bottomAnchor.constraint(equalTo: self.postContentView.bottomAnchor, constant: -Config.postPadding).isActive = true
            
            return l
        }()
    }
    
    override func setPost(_ post: Post) {
        super.setPost(post)
        
        postQuestionLabel.text = post.qaData!.question
        postAnswerLabel.text = post.qaData!.answer
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        postQuestionLabel.text = nil
        postAnswerLabel.text = nil
    }
    
    override class func estimatedHeightForPost(_ post: Post, tableView: UITableView) -> CGFloat {
        
        let width = tableView.frame.size.width - Config.postSpacing * 2 - Config.postPadding * 2
        
        let question = NSAttributedString(string: post.qaData!.question)
        let questionSize = question.boundingRect(with: CGSize(width: width, height: 9999), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], context: nil)
        
        let answer = NSAttributedString(string: post.qaData!.answer)
        let answerSize = answer.boundingRect(with: CGSize(width: width, height: 9999), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], context: nil)
        
        return super.estimatedHeightForPost(post, tableView: tableView) + questionSize.height + answerSize.height + Config.postPadding * 2
    }
}
