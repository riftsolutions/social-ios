//
//  MessageViewController.swift
//  Social
//
//  Created by Ian McDowell on 8/11/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import RealmSwift
import JSQMessagesViewController

class MessageViewController: JSQMessagesViewController {

    fileprivate var messages: Results<Message>? = nil {
        didSet {
            self.reloadJSQMessages()
        }
    }
    fileprivate var jsqMessages = [JSQMessage]()
    
    fileprivate var starterMessage = JSQMessage(senderId: "admin", senderDisplayName: "admin", date: Date.distantPast, text: "Hey Sean here, I love talking with fans, so write me a message or question, and I’ll try to get back to you soon!")
    
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    var notificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        title = "Messages"
        
        self.senderId = Fetcher.uuid
        self.senderDisplayName = Fetcher.uuid
        
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        setupBubbles()
        
        // Get posts
        let realm = try! Realm()
        messages = realm.objects(Message.self).sorted(byProperty: "date", ascending: true)
        
        notificationToken = messages!.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            self?.reloadJSQMessages()
        }
    }
    
    deinit {
        notificationToken?.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadMessages()
    }
    
    func loadMessages() {
        Fetcher.getMessages { (error) in
            
            if error != nil {
                let alert = UIAlertController(title: "Error loading messages", message: "Unable to load messages. Try again later.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func reloadJSQMessages() {
        var jsqMsgs = [JSQMessage]()
        jsqMsgs.append(self.starterMessage!)
        
        if messages != nil {
            for message in messages! {
                jsqMsgs.append(message.jsqMessage())
            }
        }
        
        self.jsqMessages = jsqMsgs
        self.finishReceivingMessage()
    }
    
    fileprivate func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
    
        Fetcher.sendMessage(text) { (success) in
            if !success {
                let alert = UIAlertController(title: "Error sending message", message: "Try again later.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            self.loadMessages()
        }
        self.finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = jsqMessages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = jsqMessages[(indexPath as NSIndexPath).item]
        
        if message.senderId == senderId {
            cell.textView!.textColor = UIColor.white
        } else {
            cell.textView!.textColor = UIColor.black
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return jsqMessages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return jsqMessages.count
    }

}
