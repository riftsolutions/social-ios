//
//  PostTextCell.swift
//  Social
//
//  Created by Ian McDowell on 7/15/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit

class PostTextCell: PostCell {
    
    var quoteImageView: UIImageView!
    var postTextView: UILabel!
    
    override func setup() {
        super.setup()
        
        self.quoteImageView = {
            let i = UIImageView(image: UIImage(named: "quote"))
            i.translatesAutoresizingMaskIntoConstraints = false
            
            i.tintColor = buttonColor
            
            self.postContentView.addSubview(i)
            i.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor, constant: Config.postPadding).isActive = true
            i.topAnchor.constraint(equalTo: self.postContentView.topAnchor, constant: Config.postPadding).isActive = true
            i.widthAnchor.constraint(equalToConstant: 24).isActive = true
            i.heightAnchor.constraint(equalToConstant: 24).isActive = true
            
            return i
        }()
        
        self.postTextView = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            
            l.numberOfLines = 0
            l.lineBreakMode = .byWordWrapping
            
            l.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
            l.textColor = Config.textColor
            
            self.postContentView.addSubview(l)
            
            l.leadingAnchor.constraint(equalTo: self.postContentView.leadingAnchor, constant: Config.postPadding).isActive = true
            l.trailingAnchor.constraint(equalTo: self.postContentView.trailingAnchor, constant: -Config.postPadding).isActive = true
            l.topAnchor.constraint(equalTo: self.quoteImageView.bottomAnchor, constant: Config.postPadding).isActive = true
            l.bottomAnchor.constraint(equalTo: self.postContentView.bottomAnchor, constant: -Config.postPadding).isActive = true
            
            return l
        }()
    }

    override func setPost(_ post: Post) {
        super.setPost(post)
        
        postTextView.text = post.textData!.text
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        postTextView.text = nil
    }
    
    override class func estimatedHeightForPost(_ post: Post, tableView: UITableView) -> CGFloat {
        
        let width = tableView.frame.size.width - Config.postSpacing * 2 - Config.postPadding * 2
        let text = NSAttributedString(string: post.textData!.text)
        
        let size = text.boundingRect(with: CGSize(width: width, height: 9999), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], context: nil)
        
        return super.estimatedHeightForPost(post, tableView: tableView) + size.height + 24 + Config.postPadding
    }
}
