//
//  Message.swift
//  Social
//
//  Created by Ian McDowell on 8/19/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import ObjectMapper
import RealmSwift
import JSQMessagesViewController

class Message: Object, Mappable {
    
    dynamic var id: String!
    dynamic var fromAdmin: Bool = false
    dynamic var user: String = ""
    dynamic var text: String = ""
    dynamic var date: Date!
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["_id"]
        fromAdmin <- map["fromAdmin"]
        text <- map["text"]
        user <- map["user"]
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        date <- (map["date"], DateFormatterTransform(dateFormatter: formatter))
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }

    func jsqMessage() -> JSQMessage {
        if fromAdmin {
            return JSQMessage(senderId: "admin", senderDisplayName: "admin", date: date, text: text)
        } else {
            return JSQMessage(senderId: user, senderDisplayName: user, date: date, text: text)
        }
    }
}
