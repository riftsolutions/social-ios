//
//  PostVideoCell.swift
//  Social
//
//  Created by Ian McDowell on 7/15/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PostVideoCell: PostCell {
    
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var playing = false
    
    fileprivate var aspectConstraint: NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                self.postContentView.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                self.postContentView.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func setup() {
        super.setup()
        
        self.player = AVPlayer()
        self.player.actionAtItemEnd = .none
        
        self.playerLayer = AVPlayerLayer(player: self.player)
        
        self.postContentView.layer.addSublayer(self.playerLayer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PostVideoCell.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.playerLayer.frame = self.postContentView.bounds
    }
    
    override func setPost(_ post: Post) {
        super.setPost(post)
        
        let aspect = CGFloat(post.videoData!.width / post.videoData!.height)
        self.aspectConstraint = NSLayoutConstraint(item: self.postContentView, attribute: .width, relatedBy: .equal, toItem: self.postContentView, attribute: .height, multiplier: aspect, constant: 0)
        
        let url = URL(string: post.videoData!.url, relativeTo: URL(string: Config.baseURL))!
        let asset = AVURLAsset(url: url)
        let keys = ["playable"]
        
        asset.loadValuesAsynchronously(forKeys: keys) { () -> Void in
            self.player.replaceCurrentItem(with: AVPlayerItem(asset: asset))
            
            if (self.active) {
                self.player.play()
                self.playing = true
            }
        }
    }
    
    @objc func playerItemDidReachEnd() {
        if (active && self.currentPost?.videoData?.loops ?? false) {
            self.player.seek(to: kCMTimeZero)
            self.playing = true
        } else {
            self.player.pause()
            self.playing = false
        }
    }
    
    override func activate() {
        if (!active) {
            self.player.play()
            self.playing = true
        }
        super.activate()
    }
    
    override func deactivate() {
        if (active) {
            self.player.pause()
            self.playing = false
        }
        super.deactivate()
    }
    
    override func action() {
        if self.player.volume != 0 {
            self.player.volume = 0
        } else {
            self.player.volume = 1
        }
        
        if !self.playing {
            self.player.seek(to: kCMTimeZero)
            self.player.play()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.player.seek(to: kCMTimeZero)
        self.player.replaceCurrentItem(with: nil)
        self.playing = false
    }
    
    // Update this when adjusting layout
    override class func estimatedHeightForPost(_ post: Post, tableView: UITableView) -> CGFloat {
        
        let ratio = CGFloat(post.videoData!.width / post.videoData!.height)
        let width = tableView.frame.size.width - Config.postSpacing * 2 - Config.postPadding * 2
        let height = ratio == 0 ? 0 : width / ratio
        
        return super.estimatedHeightForPost(post, tableView: tableView) + height
    }

}
