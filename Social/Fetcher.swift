//
//  Fetcher.swift
//  Social
//
//  Created by Ian McDowell on 7/24/16.
//  Copyright © 2016 RIFT Solutions. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import ObjectMapper

class Fetcher {
    
    class func getPosts(_ callback: @escaping (_ error: Error?) -> Void) {
        NSLog("Getting posts");
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.posts, method: .get).responseJSON { (response) in
            
            let realm = try! Realm()
            try! realm.write {
                
                if let result = response.result.value as? [[String: AnyObject]] {
                    
                    for postDict in result {
                        
                        if let post = Mapper<Post>().map(JSONObject: postDict) {
                            realm.add(post, update: true)
                        }
                    }
                    
                    var toRemove = [Post]()
                    for item in realm.objects(Post.self) {
                        var found = false
                        for post in result {
                            if post["_id"] as! String == item.id {
                                found = true
                                break
                            }
                        }
                        if !found {
                            NSLog("Post with ID \(item.id) was removed. Deleting from realm.")
                            toRemove.append(item)
                        }
                    }
                    realm.delete(toRemove)
                }
            }
            return callback(response.result.error)
        }.resume()
    }
    
    class func getMessages(_ callback: @escaping (_ error: Error?) -> Void) {
        NSLog("Getting messages");
        
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.messages, method: .get, parameters: ["uuid": uuid]).responseJSON { (response) in
            
            let realm = try! Realm()
            try! realm.write {
                if let result = response.result.value as? [[String: AnyObject]] {
                    
                    for postDict in result {
                        
                        if let message = Mapper<Message>().map(JSONObject: postDict) {
                            realm.add(message, update: true)
                        }
                    }
                }
            }
            return callback(response.result.error)
        }.resume()
    }
    
    // Mark a post as viewed.
    class func viewPost(_ post: Post) {
        NSLog("Viewing post: %@", post.id)
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.views, method: .post, parameters: ["post": post.id]).resume()
    }
    
    class func likePost(_ post: Post) {
        NSLog("Liking post: %@", post.id)
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.like, method: .post, parameters: ["post": post.id]).resume()
    }
    
    class func unlikePost(_ post: Post) {
        NSLog("Unliking post: %@", post.id)
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.unlike, method: .post, parameters: ["post": post.id]).resume()
    }
    
    
    class func registerLaunch() {
        NSLog("Registering launch")
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.registerLaunch, method: .post, parameters: ["uuid": uuid]).resume()
    }
    
    class func sendMessage(_ text: String, callback: @escaping (_ success: Bool) -> Void) {
        NSLog("Sending message: %@", text)
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.message, method: .post, parameters: ["uuid": uuid, "text": text]).response { response in
            
            callback(response.response?.statusCode == 200)
        }.resume()
    }
    
    class func setUserInfo(email: String, name: String) {
        NSLog("Sending user information: \(email) \(name)")
        Alamofire.request(Config.baseAPIURL + Config.Endpoints.userInfo, method: .post, parameters: ["uuid": uuid, "email": email, "name": name]).resume()
    }
        
        
    internal class var uuid: String {
        get {
            var uuid = UserDefaults.standard.string(forKey: "uuid")
            if uuid == nil {
                uuid = UIDevice.current.identifierForVendor?.uuidString ?? UUID().uuidString
                UserDefaults.standard.set(uuid!, forKey: "uuid")
            }
            return uuid!
        }
    }
}
